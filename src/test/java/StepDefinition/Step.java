package StepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Step {
    WebDriver driver;


    @Given("^Open the chrome and launch the application$")
    public void Open_the_chrome_and_launch_the_application() throws Throwable {
//        System.out.println("This step open the chrome and launch the application");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/v4");
    }

    @When("^Enter the username and password$")
    public void Enter_the_username_and_password() throws Throwable{
//        System.out.println("This step enter the username and password");
        driver.findElement(By.name("uid")).sendKeys("username");
        driver.findElement(By.name("password")).sendKeys("secret");
    }

    @Then("^Reset the credential$")
    public void Reset_the_credential() throws Throwable{
        System.out.println("This step click reset the credential");
        driver.findElement(By.name("btnReset")).click();

        Thread.sleep(1000);
        driver.quit();
    }


}
