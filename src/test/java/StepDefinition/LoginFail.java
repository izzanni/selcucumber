package StepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginFail {
    WebDriver driver;
    //WebDriverWait wait = new WebDriverWait(driver, 5);

    @Given("^User navigates to login page$")
    public void User_navigates_to_login_page() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/v4");
    }

    @When("^User enter invalid userID and password$")
    public void User_enter_invalid_userID_and_password() throws Throwable {
        driver.findElement(By.name("uid")).sendKeys("user");
        driver.findElement(By.name("password")).sendKeys("secret123");
        Thread.sleep(1000);
    }

    @Then("^Wrong message pop up$")
    public void Wrong_message_pop_up() throws Throwable {
        System.out.println("This step user login fail");
        driver.findElement(By.name("btnLogin")).click();
        driver.switchTo().alert().accept();
        Thread.sleep(1000);
        driver.quit();
    }
}
