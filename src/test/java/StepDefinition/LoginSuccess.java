package StepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertTrue;

public class LoginSuccess {
    WebDriver driver;


    @Given("^User go to login page$")
    public void User_go_to_login_page() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/v4");
    }

    @When("^User enter valid userID and password$")
    public void User_enter_valid_userID_and_password() throws Throwable {
        driver.findElement(By.name("uid")).sendKeys("mngr418205");
        driver.findElement(By.name("password")).sendKeys("bumeqEq");
        Thread.sleep(1000);
    }

    @Then("^User is navigated to homepage$")
    public void User_is_navigated_to_homepage() throws Throwable {
        System.out.println("This step user login successful");
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.findElement(By.name("btnLogin")).click();
        WebElement id = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[3]/td")));
        assertTrue(id.isDisplayed());
        Thread.sleep(1000);
        driver.quit();

    }
}
